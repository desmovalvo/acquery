#!/usr/bin/python3

# requirements
import os
import yaml
import logging
from .QueryBuilderExceptions import *

# main class
class QueryBuilder:

    # class attributes
    submodules = {}
    ycontent = None
    logger = None
    
    def __init__(self, confFile, logLevel = 40):

        """
        The constructor of the QueryBuilder class. 

        Parameters
        ----------
        confFile : str
            The name (with relative or absolute path) of the config file
        logLevel : int
            The desired level of debugging information (default = 40)

        """
        
        # logger
        self.logger = logging.getLogger("sepaLogger")
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logLevel)
        self.logger.setLevel(logLevel)

        # debug
        self.logger.debug("Initializing QueryBuilder")

        # read config file
        try:

            # open file
            self.ycontent = yaml.load(open(confFile, "r"))

            # scan for content providers
            for cp in self.ycontent["providers"]:

                # save the new provider
                self.logger.debug("Found content provider %s" % cp)                
                self.submodules[cp] = {}
                self.submodules[cp]["queries"] = {}
                self.submodules[cp]["path"] = self.ycontent["providers"][cp]["path"]

                # now scan for queries
                for f in os.listdir(self.submodules[cp]["path"]):
                    filename = os.fsdecode(f)
                    if filename.endswith(".rq"):
                        
                        # store the query
                        k = f.upper().split(".")[0]
                        self.logger.debug("Adding mapping %s to %s" % (k, cp))
                        with open(self.submodules[cp]["path"] + f, "r") as fr:                            
                            self.submodules[cp]["queries"][k] = fr.read()
                                           
        except FileNotFoundError:
            self.logger.critical("File not found!")
            raise QueryBuilderException("File not found!")
        
        except KeyError:
            self.logger.critical("Wrong configuration!")
            raise QueryBuilderException("Wrong configuration!")
                
                            
    def get_query(provider, qname, binding = None):

        """
        This method allows to retrieve a SPARQL-generate query for the
        content provider specified. The optional parameter 'binding' allows
        to put the search URI inside the query. Otherwise, only the template
        is returned to the user.

        Parameters
        ----------
        provider : str
            The name of the content provider (e.g. freesound)
        qname : str
            The name of the query to retrieve
        binding : str
            The search URI to be put in the SPARQL-generate mapping

        """
        
        if provider in self.submodules:
            if qname in self.submodules[provider]["queries"][qname]:
                if binding:
                    return self.submodules[provider]["queries"][qname].replace("?searchURI", binding)                    
                else:
                    return self.submodules[provider]["queries"][qname]
            else:
                raise QueryBuilderException("Query not found!")
        else:
            raise QueryBuilderException("Provider not found!")
