from setuptools import setup

setup(name='acquery',
      version='0.1',
      description='Conversion library for AudioCommons',
      url='https://gitlab.com/desmovalvo/acquery.git',
      author='Fabio Viola',
      author_email='fabio.viola@unibo.it',
      license='GPL',
      packages=['acquery'],
      zip_safe=False)
